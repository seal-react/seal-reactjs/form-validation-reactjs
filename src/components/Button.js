import React from "react";

const Button = ({
  title = "Button",
  backgroundColor = "#27ae60",
  ...props
}) => {
  return (
    <button
      style={{
        background: backgroundColor,
        border: 0,
        color: "white",
        borderRadius: 5,
        cursor: "pointer",
      }}
      {...props}>
      {title}
    </button>
  );
};

export default Button;
