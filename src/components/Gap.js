import React from "react";

const Gap = ({ top, bottom, right, left }) => {
  return (
    <div
      style={{
        marginTop: top,
        marginBottom: bottom,
        marginRight: right,
        marginLeft: left,
      }}
    />
  );
};

export default Gap;
